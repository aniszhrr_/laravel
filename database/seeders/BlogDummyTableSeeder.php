<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as faker;

class BlogDummyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1,100) as $index){
            DB::table('blog')->insert([
                'title' => $faker->sentence,
                'content' => $faker->paragraph,
                'category' => $faker->jobTitle
            ]);
        }
    }
}
