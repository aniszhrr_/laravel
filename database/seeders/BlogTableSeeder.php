<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title' => 'Susul Dita Karang, SM akan Debutkan Trainee asal Indonesia?',
            'content' => 'Baru-baru ini, ramai diperbincangkan bahwa SM Entertainment akan segera mendebutkan trainee asal Indonesia untuk girl band dan boy band terbaru mereka. Hal ini juga dipicu oleh antusias tinggi dari para pecinta Kpop di Tanah Air.',
            'category' => 'K-Pop Artist'
        ]);
    }
}
